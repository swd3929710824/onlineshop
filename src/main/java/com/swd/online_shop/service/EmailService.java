package com.swd.online_shop.service;

import java.util.Map;

public interface EmailService {
    void sendHtml(String subject, String emailTo, String htmlContent);

    void sendText(String subject, String emailTo, String content);

    void sendTemplate(String subject, String emailTo, String template, Map<String, Object> variables);
}


