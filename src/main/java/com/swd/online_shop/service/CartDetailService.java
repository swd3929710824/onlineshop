package com.swd.online_shop.service;

import com.swd.online_shop.entity.Cartdetail;
import com.swd.online_shop.entity.User;

import java.util.List;

public interface CartDetailService {
    List<Cartdetail> getListCartItem(int cartId);

    void removeCartDetail(int orderDetailId, User user);

    void updateCartDetail(int orderDetailId, int quantity, User user);
}
