package com.swd.online_shop.service;

import com.swd.online_shop.entity.Order;
import com.swd.online_shop.entity.Orderdetail;
import org.springframework.data.domain.Page;

public interface OrderService {
    Page<Order> findOrders(int page, int size, String status, String search);

    Order getOrderById(Integer orderId);

    Object getDetailsByOrderId(Integer orderId);

    Orderdetail getOrderDetailById(Integer orderDetailId);

    void saveOrderdetail(Orderdetail orderdetail);

    void deleteOrderById(Integer orderId);

    void addOrderDetail(Orderdetail orderDetail);

    Page<Order> findOrdersByUserId(int id, int page, int size, String status);
}
