package com.swd.online_shop.service;

import com.swd.online_shop.entity.Cart;
import com.swd.online_shop.entity.Cartdetail;
import com.swd.online_shop.entity.User;

import java.math.BigDecimal;
import java.util.List;

public interface CartService {
        public Cart findCartByUser(User user);

        BigDecimal TotalCartPrice(List<Cartdetail> cartdetailList);

        Cartdetail AddToCart(int productId, int quantity, User user);
}
