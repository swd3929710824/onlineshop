package com.swd.online_shop.service;

import com.swd.online_shop.dto.Account;
import com.swd.online_shop.entity.User;
import org.springframework.data.domain.Page;
import java.util.Optional;

public interface UserService {

    User register(Account account);

    User authenticate(Account account);

    User verification(String email);

    User sendResetMail(String email);

    User resetPassword(String email, String password);

    Optional<User> findUserById(int userId);

    Page<User> getUsers(int page, int size, String status, String search);

    void saveUser(User user);

    User getUserById(int id);

}