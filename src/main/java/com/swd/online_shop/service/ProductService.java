package com.swd.online_shop.service;

import com.swd.online_shop.entity.Product;
import org.springframework.data.domain.Page;


public interface ProductService {

    Page<Product> getAllProduct(int pageNo, int pageSize);

    Product getProductById(int id);

    Page<Product> findProducts(int page, int size, String status, String search);

    Product getProductDetail(Integer id);

    Page<Product> findProductsByType(int pageNo, int pageSize, String brand, String category, String search);
}
