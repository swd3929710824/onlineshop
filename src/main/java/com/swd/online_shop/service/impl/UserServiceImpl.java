package com.swd.online_shop.service.impl;

import com.swd.online_shop.dto.Account;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.mapper.UserMapper;
import com.swd.online_shop.repository.SettingRepository;
import com.swd.online_shop.repository.UserRepository;
import com.swd.online_shop.service.EmailService;
import com.swd.online_shop.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    private final UserRepository userRepository;
    private final SettingRepository settingRepository;

    private final EmailService emailService;

    @Override
    public User register(Account account) {
        // Validate input
        if (account.getEmail() == null || account.getPassword() == null) {
            throw new IllegalArgumentException("Email and password must not be null");
        }

        // Check if email already exists
        if (userRepository.findByEmail(account.getEmail()) != null) {
            throw new IllegalArgumentException("Email already in use");
        }

        // Convert Account to User
        User newUser = userMapper.userFromAccount(account);

        // Send verification email
        Map<String, Object> variables = new HashMap<>();
        variables.put("email", account.getEmail());
        variables.put("message", "Thank you for joining our platform.");
        emailService.sendTemplate("This is a verification email", account.getEmail(), "email/emailform", variables);
        // Save the new user
        return userRepository.save(newUser);
    }
    @Override
    public User authenticate(Account account) {
        return userRepository.findByEmailAndPasswordAndStatus(account.getEmail(), account.getPassword(), "active").orElse(null);
    }


    @Override
    public User verification(String email) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setStatus("active");
            var role = settingRepository.findBySettingNameAndSettingType("role", "customer");
            user.setRole(role);
            return userRepository.save(user);
        }
        return null;
    }

    @Override
    public User sendResetMail(String email) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("email", email);
        variables.put("message", "Thank you for joining our platform.");
        emailService.sendTemplate("This is a reset password email", email, "email/resetemail", variables);
        return null;
    }

    @Override
    public User resetPassword(String email, String password) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setPassword(password);
            return userRepository.save(user);
        }
        return null;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(int id) {
        return userRepository.findById(id).orElse(null);
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    public Optional<User> findUserById(int userId) {
        return userRepository.findById(userId);
    }

    public Page<User> getUsers(int page, int size, String status, String search) {
        Pageable pageable = PageRequest.of(page, size);
        if (status != null && !status.isEmpty() && !status.equals("all")) {
            return userRepository.findByStatus(status, pageable);
        } else if (search != null && !search.isEmpty()) {
            return userRepository.findByUserNameContaining(search, pageable);
        } else {
            return userRepository.findAll(pageable);
        }
    }
}
