package com.swd.online_shop.service.impl;

import com.swd.online_shop.entity.Cart;
import com.swd.online_shop.entity.Cartdetail;
import com.swd.online_shop.entity.Product;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.repository.CartRepository;
import com.swd.online_shop.repository.CartdetailRepository;
import com.swd.online_shop.repository.ProductRepository;
import com.swd.online_shop.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {
    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final CartdetailRepository cartdetailRepository;

    @Override
    public Cart findCartByUser(User user){
        Cart cart = cartRepository.findCartByUser(user);
        if(cart!=null){return cart;}
        else {
            cart = new Cart();
            cart.setUser(user);
            cartRepository.save(cart);
            return cart;
        }
    }

    @Override
    public BigDecimal TotalCartPrice(List<Cartdetail> cartdetailList) {
        BigDecimal total = BigDecimal.ZERO;
        for (Cartdetail cartdetail : cartdetailList) {
            BigDecimal productPrice = productRepository.findById(cartdetail.getProduct().getId()).orElseThrow().getPrice();
            BigDecimal quantity = new BigDecimal(cartdetail.getQuantity());
            total = total.add(productPrice.multiply(quantity));
        }
        return total;
    }

    public Cartdetail AddToCart(int productId, int quantity, User user) {
        Cart cart = cartRepository.findCartByUser(user);
        if(cart == null){
                cart = new Cart();
                cart.setUser(user);
                cartRepository.save(cart);
                Product product = productRepository.findById(productId);
                Cartdetail cartdetail = new Cartdetail();
                cartdetail.setProduct(product);
                cartdetail.setCart(cart);
                cartdetail.setQuantity(quantity);
                return cartdetailRepository.save(cartdetail);

        }else{
            Cartdetail orderdetail = cartdetailRepository.findCartdetailByProductIdAndCart(productId, cart);
            if (orderdetail != null) {
                orderdetail.setQuantity(orderdetail.getQuantity() + quantity);
                return cartdetailRepository.save(orderdetail);
            }else {
                Product product = productRepository.findById(productId);
                Cartdetail cartdetail = new Cartdetail();
                cartdetail.setProduct(product);
                cartdetail.setCart(cart);
                cartdetail.setQuantity(quantity);
                return cartdetailRepository.save(cartdetail);
            }
        }
    }


}
