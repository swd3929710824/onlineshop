package com.swd.online_shop.service.impl;

import com.swd.online_shop.entity.Order;
import com.swd.online_shop.entity.Orderdetail;
import com.swd.online_shop.repository.OrderRepository;
import com.swd.online_shop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;

    public Page<Order> findOrders(int page, int size, String status, String search) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("orderDate").descending());
        if (status != null && !status.isEmpty() && !status.equals("all")) {
            return orderRepository.findByStatus(status, pageable);
        } else if (search != null && !search.isEmpty()) {
            return orderRepository.findByUser_UserNameContaining(search, pageable);
        } else {
            return orderRepository.getOrderList(pageable);
        }
    }

    public Order getOrderById(Integer orderId) {
        return orderRepository.findByOrderId(orderId);
    }

    public List<Orderdetail> getDetailsByOrderId(Integer orderId) {
        return orderRepository.findDetailByOrderId(orderId);
    }

    public Orderdetail getOrderDetailById(Integer orderDetailId) {
        Optional<Orderdetail> optionalOrderdetail = orderRepository.findOrderdetailById(orderDetailId);
        return optionalOrderdetail.orElse(null);
    }

    public void saveOrderdetail(Orderdetail orderdetail) {
        orderRepository.saveOrderdetail(orderdetail.getId(), orderdetail.getOrder(), orderdetail.getProduct(), orderdetail.getQuantity(), orderdetail.getPrice());
    }

    public void deleteOrderById(Integer orderId) {
        orderRepository.deleteById(orderId);
    }

    @Override
    public void addOrderDetail(Orderdetail orderDetail) {

    }

    @Override
    public Page<Order> findOrdersByUserId(int id, int page, int size, String status) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("orderDate").descending());
        if (status != null && !status.isEmpty() && !status.equals("all")) {
            return orderRepository.findUserOrderByStatus(id ,status, pageable);
        }else {
            return orderRepository.getUserOrderList(id, pageable);
        }
    }
}
