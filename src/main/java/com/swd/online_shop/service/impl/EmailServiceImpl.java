package com.swd.online_shop.service.impl;

import com.swd.online_shop.service.EmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender emailSender;
    private final TemplateEngine templateEngine;

    @SneakyThrows
    @Override
    @Async
    public void sendHtml(String subject, String emailTo, String htmlContent) {
        send(subject, emailTo, htmlContent, true);
    }

    @SneakyThrows
    @Override
    @Async
    public void sendText(String subject, String emailTo, String content) {
        send(subject, emailTo, content, false);
    }

    @SneakyThrows
    @Override
    @Async
    public void sendTemplate(String subject, String emailTo, String template, Map<String, Object> variables) {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(variables);
        String htmlContent = templateEngine.process(template, thymeleafContext);
        send(subject, emailTo, htmlContent, true);
    }

    private void send(String subject, String emailTo, String content, boolean isHtmlFormat) throws MessagingException {
        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
        message.setTo(emailTo);
        message.setSubject(subject);
        message.setText(content, isHtmlFormat);
        emailSender.send(mimeMessage);
    }
}
