package com.swd.online_shop.service.impl;

import com.swd.online_shop.entity.Product;
import com.swd.online_shop.repository.ProductRepository;
import com.swd.online_shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    public List<Product> findAllProduct(){
        return productRepository.findAll();
    }

    @Override
    public Page<Product> getAllProduct(int pageNo, int pageSize){
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Product> products = productRepository.findAll(pageable);
        return products;
    }

    @Override
    public Product getProductById(int id){
        return productRepository.findById(id);
    }

    @Override
    public Page<Product> findProducts(int page, int size, String status, String search) {
        Pageable pageable = PageRequest.of(page, size);
        if (status != null && !status.isEmpty() && !status.equals("all")) {
            return productRepository.findByStatus(status, pageable);
        } else if (search != null && !search.isEmpty()) {
            return productRepository.findByProductNameContaining(search, pageable);
        } else {
            return productRepository.getProductList(pageable);
        }
    }

    @Override
    public Product getProductDetail(Integer id) {
        return productRepository.findProduct(id);
    }

    @Override
    public Page<Product> findProductsByType(int pageNo, int pageSize, String brand, String category, String search) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        if (brand != null && !brand.isEmpty()) {
            return productRepository.findBySettingName(brand, pageable);
        } else if (category != null && !category.isEmpty()) {
            return productRepository.findBySettingName(category, pageable);
        } else if (search != null && !search.isEmpty()) {
            return productRepository.findByProductNameContaining(search, pageable);
        } else {
            return productRepository.getProductList(pageable);
        }
    }
}
