package com.swd.online_shop.service.impl;

import com.swd.online_shop.entity.Cart;
import com.swd.online_shop.entity.Cartdetail;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.repository.CartRepository;
import com.swd.online_shop.repository.CartdetailRepository;
import com.swd.online_shop.service.CartDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CartDetailServiceImpl implements CartDetailService {

    private final CartdetailRepository cartdetailRepository;
    private final CartRepository cartRepository;
    @Override
    public List<Cartdetail> getListCartItem(int cartId){
        List<Cartdetail> cartdetails = cartdetailRepository.findByCartId(cartId);
        return cartdetails;
    }

    public void removeCartDetail(int orderDetailId, User user) {
        // Find the cart associated with the user
        Cart cart = cartRepository.findCartByUser(user);

        if (cart != null) {
            // Find the cart detail to be removed
            Cartdetail orderdetail = cartdetailRepository.findByIdAndCart(orderDetailId, cart);

            if (orderdetail != null) {
                // Remove the cart detail from the repository
                cartdetailRepository.delete(orderdetail);
            }
        }
    }

    public void updateCartDetail(int orderDetailId, int quantity, User user) {
        // Find the cart associated with the user
        Cart cart = cartRepository.findCartByUser(user);

        if (cart != null) {
            // Find the cart detail to be removed
            Cartdetail orderdetail = cartdetailRepository.findByIdAndCart(orderDetailId, cart);

            if (orderdetail != null) {
                // Remove the cart detail from the repository
                orderdetail.setQuantity(quantity);
                cartdetailRepository.save(orderdetail);
            }
        }
    }


}
