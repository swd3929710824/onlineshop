package com.swd.online_shop.repository;

import com.swd.online_shop.entity.Cart;
import com.swd.online_shop.entity.Cartdetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartdetailRepository extends JpaRepository<Cartdetail, Integer> {
    List<Cartdetail> findByCartId(int cartId);

    Cartdetail findCartdetailByProductIdAndCart(int productid , Cart cart);

    Cartdetail findByIdAndCart(int orderDetailId , Cart cart);
}