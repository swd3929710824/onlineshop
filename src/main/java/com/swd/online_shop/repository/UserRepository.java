package com.swd.online_shop.repository;

import com.swd.online_shop.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmailAndPasswordAndStatus(String email, String password, String status);

    User findByEmail(String email);

    Page<User> findByUserNameContaining(String search, Pageable pageable);

    Page<User> findByStatus(String status, Pageable pageable);
}
