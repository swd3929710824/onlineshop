package com.swd.online_shop.repository;

import com.swd.online_shop.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findById(int id);

    @Query("SELECT p FROM Product p JOIN FETCH p.brand JOIN FETCH p.category WHERE p.status = :status")
    Page<Product> findByStatus(@Param("status") String status, Pageable pageable);

    @Query("SELECT p FROM Product p JOIN FETCH p.brand JOIN FETCH p.category WHERE p.productName LIKE %:search%")
    Page<Product> findByProductNameContaining(@Param("search") String search, Pageable pageable);

    @Query("SELECT p FROM Product p JOIN FETCH p.brand JOIN FETCH p.category")
    Page<Product> getProductList(Pageable pageable);

    @Query("SELECT p FROM Product p JOIN FETCH p.brand JOIN FETCH p.category WHERE p.id = :ProductId")
    Product findProduct(@Param("ProductId") Integer ProductId);

    @Query("SELECT p FROM Product p JOIN FETCH p.brand JOIN FETCH p.category WHERE p.brand.settingName LIKE %:settingName% OR p.category.settingName LIKE %:settingName%")
    Page<Product> findBySettingName(@Param("settingName")String settingName, Pageable pageable);
}
