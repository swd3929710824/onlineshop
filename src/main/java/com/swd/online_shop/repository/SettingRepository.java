package com.swd.online_shop.repository;

import com.swd.online_shop.entity.Setting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SettingRepository extends JpaRepository<Setting, Integer> {
  Setting findBySettingNameAndSettingType(String name, String type);

  List<Setting> findAllBySettingType(String type);
}