package com.swd.online_shop.repository;

import com.swd.online_shop.entity.Order;
import com.swd.online_shop.entity.Orderdetail;
import com.swd.online_shop.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Integer>{

    void deleteById(Integer id);

    @Query("SELECT o FROM Order o JOIN FETCH o.user")
    Page<Order> getOrderList(Pageable pageable);

    @Query("SELECT o FROM Order o JOIN FETCH o.user WHERE o.status = :status")
    Page<Order> findByStatus(@Param("status") String status, Pageable pageable);

    @Query("SELECT o FROM Order o JOIN FETCH o.user WHERE o.user.userName LIKE %:username% OR o.user.email LIKE %:username%")
    Page<Order> findByUser_UserNameContaining(@Param("username") String username, Pageable pageable);

    @Query("SELECT o FROM Order o JOIN FETCH o.user WHERE o.id = :Id")
    Order findByOrderId(@Param("Id")Integer Id);

    @Query("SELECT od FROM Orderdetail od JOIN FETCH od.order o JOIN FETCH od.product p JOIN FETCH o.user WHERE o.id = :orderId")
    List<Orderdetail> findDetailByOrderId(@Param("orderId") Integer orderId);

    @Query("SELECT od FROM Orderdetail od JOIN FETCH od.order o JOIN FETCH od.product p JOIN FETCH o.user WHERE od.id = :orderDetailId")
    Optional<Orderdetail> findOrderdetailById(@Param("orderDetailId") Integer orderDetailId);

    @Modifying
    @Query("UPDATE Orderdetail od SET od.order = :order, od.product = :product, od.quantity = :quantity, od.price = :price WHERE od.id = :id")
    void saveOrderdetail(@Param("id") Integer id, @Param("order") Order order, @Param("product") Product product, @Param("quantity") Integer quantity, @Param("price") BigDecimal price);

    @Query("SELECT o FROM Order o JOIN FETCH o.user WHERE o.user.id = :id AND o.status = :status")
    Page<Order> findUserOrderByStatus(@Param("id") Integer id,@Param("status") String status, Pageable pageable);

    @Query("SELECT o FROM Order o JOIN FETCH o.user WHERE o.user.id = :id")
    Page<Order> getUserOrderList(@Param("id") Integer id, Pageable pageable);
}
