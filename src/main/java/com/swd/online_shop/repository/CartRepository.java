package com.swd.online_shop.repository;

import com.swd.online_shop.entity.Cart;
import com.swd.online_shop.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    Cart findCartByUser(User user);
}