package com.swd.online_shop.base;

import jakarta.persistence.criteria.JoinType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class SearchCriterion {

    private String key;
    private Object value;
    private Operation operation;
    private String joinTable;
    private JoinType joinType;

    public enum Operation {
        GREATER_THAN,
        LESS_THAN,
        GREATER_THAN_EQUAL,
        LESS_THAN_EQUAL,
        NOT_EQUAL,
        EQUAL,
        LIKE,
        LIKE_START,
        LIKE_END,
        NULL,
        NOT_NULL,
        IN,
        BETWEEN
    }
}
