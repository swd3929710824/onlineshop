package com.swd.online_shop.controller;


import com.swd.online_shop.entity.Cart;
import com.swd.online_shop.entity.Cartdetail;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.service.CartDetailService;
import com.swd.online_shop.service.CartService;
import com.swd.online_shop.service.ProductService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;
    private final CartDetailService cartDetailService;
    private final ProductService productService;


    @GetMapping("/cart")
    public String cart(Model model, HttpSession session) {
        User user = (User) session.getAttribute("authenticated");
        if (user == null) {
            return "redirect:/login";
        }
        Cart cart = cartService.findCartByUser(user);
        List<Cartdetail> cartdetails = cartDetailService.getListCartItem(cart.getId());
        model.addAttribute("cart", cart);
        model.addAttribute("cartdetails", cartdetails);
        model.addAttribute("productService", productService);
        BigDecimal cartTotal = cartService.TotalCartPrice(cartdetails);
        model.addAttribute("cartTotal", cartTotal);
        return "cart/cart";
    }
    @GetMapping("/cart/add")
    public String addCart(Model model, HttpSession session, @RequestParam("productId") Integer  productId, @RequestParam("quantity") Integer quantity ) {
        User user = (User) session.getAttribute("authenticated");
        Cartdetail cartdetail = cartService.AddToCart(productId, quantity, user);
        return "redirect:/public_page";
    }

    @GetMapping("/cart/remove")
    public String removeCart(Model model, HttpSession session, @RequestParam("orderDetailId") Integer orderDetailId) {
        User user = (User) session.getAttribute("authenticated");
        cartDetailService.removeCartDetail(orderDetailId, user);
        return "redirect:/cart";
    }

    @GetMapping("/cart/update")
    public String updateCart(Model model, HttpSession session, @RequestParam("cartdetailId") Integer  cartdetailId, @RequestParam("quantity") Integer quantity ) {
        User user = (User) session.getAttribute("authenticated");
        cartDetailService.updateCartDetail(cartdetailId, quantity, user);
        return "redirect:/cart";
    }
}
