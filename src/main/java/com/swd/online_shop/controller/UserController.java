package com.swd.online_shop.controller;
import com.swd.online_shop.dto.Account;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.service.UserService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;
import java.util.logging.Logger;
@Controller
@RequiredArgsConstructor

public class UserController {

    private final UserService userService;


    @GetMapping("users/list")
    public String listUsers(Model model,
                            @RequestParam(defaultValue = "0") int page,
                            @RequestParam(name = "status", required = false) String status,
                            @RequestParam(name = "search", required = false) String search) {
        int size = 5;
        if (status == null) {
            status = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        if (search == null) {
            search = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        Page<User> userPage = userService.getUsers(page, size, status, search);
        model.addAttribute("status", status);
        model.addAttribute("search", search);
        model.addAttribute("userPage", userPage);
        model.addAttribute("currentPage", page);
        return "user/list";
    }

    @RequestMapping(value = "users/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam("userId") int userId, Model model) {
        Optional<User> userEdit = userService.findUserById(userId);
        userEdit.ifPresent(user -> model.addAttribute("user", user));
        return "user/edit";
    }

    @RequestMapping("users/add")
    public String addUser(Model model) {
        model.addAttribute("user", new User());
        return "user/add";
    }

    @RequestMapping(value = "users/save", method = RequestMethod.POST)
    public String save(User user) {
        userService.saveUser(user);
        return "redirect:/users/list";
    }

    @GetMapping("/users/view")
    public String viewUser(@RequestParam int userId, Model model) {
        User user = userService.getUserById(userId);
        model.addAttribute("user", user);
        return "user/detail";
    }

    Logger logger = Logger.getLogger(getClass().getName());

    @GetMapping("/register")
    public String getRegister(Model model) {
        model.addAttribute("registerRequest", new Account());
        return "home/register_page";
    }

    @GetMapping("/login")
    public String getLogin(Model model) {
        model.addAttribute("loginRequest", new Account());
        return "home/login_page";
    }

    @GetMapping("/logout")
    public String getLogout(HttpSession session){
        session.invalidate();
        return "redirect:/login";
    }

    @GetMapping("/forgotpassword")
    public String getForgotPassword(Model model) {
        model.addAttribute("forgotpasswordRequest", new Account());
        return "home/forgotpassword_page";
    }

    @GetMapping("/passwordreset")
    public String getPasswordReset(Model model, @RequestParam("email") String email) {
        model.addAttribute("email", email);
        model.addAttribute("passwordresetRequest", new Account());
        return "home/passwordreset_page";
    }

    @GetMapping("/send_verification")
    public String sendVerification(@RequestParam("email") String email, RedirectAttributes redirectAttributes) {
        logger.info("mail: " + email);
        redirectAttributes.addFlashAttribute("message", "Verification email sent successfully!");
        User user = userService.verification(email);
        return "redirect:/login";
    }


    @PostMapping("/register")
    public String register(@ModelAttribute Account account) {
        logger.info("register request :" + account);
        User registerUser = userService.register(account);
        return registerUser != null ? "redirect:/login" : "redirect:/register";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute Account account, HttpSession session) {
        logger.info("login request :" + account);
        User authenticated = userService.authenticate(account);
        if (authenticated != null) {
            session.setAttribute("authenticated", authenticated);
            return "redirect:public_page";
        } else {
            return "redirect:login";
        }
    }



    @PostMapping("/forgotpassword")
    public String forgotPassword(@ModelAttribute Account account) {
        logger.info("email: " + account.getEmail());
        User user = userService.sendResetMail(account.getEmail());
        return "redirect:/forgotpassword";
    }

    @PostMapping("/reset-password")
    public String resetPassword(@ModelAttribute Account account , @RequestParam("email") String email ) {
        logger.info("reset password request :" + account.getPassword());
        User user = userService.resetPassword(email, account.getPassword());
        return "redirect:/login";
    }

}
