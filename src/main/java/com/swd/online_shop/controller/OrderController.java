package com.swd.online_shop.controller;

import com.swd.online_shop.entity.Order;
import com.swd.online_shop.entity.Orderdetail;
import com.swd.online_shop.entity.Product;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.service.OrderService;
import com.swd.online_shop.service.ProductService;
import jakarta.servlet.http.HttpSession;
import com.swd.online_shop.entity.*;
import com.swd.online_shop.service.*;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final ProductService productService;
    private final CartService cartService;
    private final CartDetailService cartDetailService;

    @GetMapping("/list")
    public String showOrderList(Model model,
                                @RequestParam(name = "page", defaultValue = "0") int page,
                                @RequestParam(name = "status", required = false) String status,
                                @RequestParam(name = "search", required = false) String search) {
        int size = 5;
        if (status == null) {
            status = "";
        }
        if (search == null) {
            search = "";
        }
        Page<Order> orderPage = orderService.findOrders(page, size, status, search);
        for (Order order : orderPage) {
            Hibernate.initialize(order.getUser());
        }
        model.addAttribute("status", status);
        model.addAttribute("search", search);
        model.addAttribute("orderPage", orderPage);
        return "order/list";
    }

    @GetMapping("/history")
    public String showOrderHistory(Model model, HttpSession session,
                                   @RequestParam(name = "page", defaultValue = "0") int page,
                                   @RequestParam(name = "status", required = false) String status) {
        User user = (User) session.getAttribute("authenticated");
        int size = 5;
        if (status == null) {
            status = "";
        }
        Page<Order> orderPage = orderService.findOrdersByUserId( user.getId(),page, size, status);
        for (Order order : orderPage) {
            Hibernate.initialize(order.getUser());
        }
        model.addAttribute("status", status);
        model.addAttribute("orderPage", orderPage);
        return "order/history";
    }

    @GetMapping("/details")
    public String showOrderDetails(@RequestParam("orderId") Integer orderId, Model model) {
        Order order = orderService.getOrderById(orderId);
        if (order == null) {
            // Handle case where order is not found
            return "redirect:/orders/list";
        }
        model.addAttribute("order", order);
        model.addAttribute("orderDetails", orderService.getDetailsByOrderId(orderId));
        return "order/detail";
    }

    @GetMapping("/detail/{orderId}/edit/{orderDetailId}")
    public String showOrderDetailEditForm(@PathVariable("orderId") Integer orderId,
                                          @PathVariable("orderDetailId") Integer orderDetailId,
                                          Model model) {
        Order order = orderService.getOrderById(orderId);
        model.addAttribute("order", order);
        Orderdetail orderDetail = orderService.getOrderDetailById(orderDetailId);
        model.addAttribute("orderdetail", orderDetail);
        return "order/edit";
    }

    @GetMapping("/cancel")
    public ResponseEntity<String> cancelOrder(@RequestParam("orderId") Integer orderId) {
        orderService.deleteOrderById(orderId);
        return ResponseEntity.ok("Order with ID " + orderId + " has been cancelled successfully.");
    }

    @GetMapping("/checkout")
    public String checkout(Model model, HttpSession session ) {
        model.addAttribute("order", new Order());
        User user = (User) session.getAttribute("authenticated");
        if (user == null) {
            return "redirect:/login";
        }
        Cart cart = cartService.findCartByUser(user);
        List<Cartdetail> cartdetails = cartDetailService.getListCartItem(cart.getId());
        model.addAttribute("cart", cart);
        model.addAttribute("cartdetails", cartdetails);
        model.addAttribute("productService", productService);
        BigDecimal cartTotal = cartService.TotalCartPrice(cartdetails);
        model.addAttribute("cartTotal", cartTotal);
        return "order/checkout";
    }

    @PostMapping("/{orderId}/details/edit/{orderDetailId}")
    public String editOrderDetail(@PathVariable("orderId") Integer orderId,
                                  @PathVariable("orderDetailId") Integer orderDetailId,
                                  @ModelAttribute("orderdetail") Orderdetail orderdetail,
                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            // Handle validation errors if needed
        }
        orderdetail.setId(orderDetailId);
        orderdetail.setOrder(orderService.getOrderById(orderId));
        orderService.saveOrderdetail(orderdetail);
        return "redirect:/orders/detail/" + orderId;
    }

}
