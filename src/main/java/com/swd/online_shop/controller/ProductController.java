package com.swd.online_shop.controller;

import com.swd.online_shop.entity.Product;
import com.swd.online_shop.entity.Setting;
import com.swd.online_shop.repository.SettingRepository;
import com.swd.online_shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final SettingRepository settingRepository;

    @GetMapping("/productlistdetail")
    public String getProductDetail(Model model,@RequestParam("productId") int id) {
        Product product = productService.getProductDetail(id);
        model.addAttribute("product", product);
        return "product/detail";
    }

    @GetMapping("/productdetail")
    public String getShop(Model model, @RequestParam("id") int id) {
        Product product = productService.getProductById(id);
        List<Setting> category = settingRepository.findAllBySettingType("category");
        List<Setting> brand = settingRepository.findAllBySettingType("brand");
        model.addAttribute("product", product);
        model.addAttribute("category", category);
        model.addAttribute("brand", brand);
        return "shop/product-details";
    }

    @GetMapping("/productlist")
    public String showProductList(Model model,
                                @RequestParam(name = "page", defaultValue = "0") int page,
                                @RequestParam(name = "status" , required = false) String status,
                                @RequestParam(name = "search" , required = false) String search) {
        int size = 5;
        if (status == null) {
            status = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        if (search == null) {
            search = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        Page<Product> productPage = productService.findProducts(page,size, status, search);
        model.addAttribute("status", status);
        model.addAttribute("search", search);
        model.addAttribute("productPage", productPage);
        return "product/list";
    }

}
