package com.swd.online_shop.controller;

import com.swd.online_shop.entity.Product;
import com.swd.online_shop.entity.Setting;
import com.swd.online_shop.entity.User;
import com.swd.online_shop.repository.SettingRepository;
import com.swd.online_shop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
@RequiredArgsConstructor
public class HomeController {

    private final ProductService productService;

    private final SettingRepository settingRepository;

//    @GetMapping("/public_page")
//    public String getPublicPage(Model model,
//                                @RequestParam(value = "pageNo", defaultValue = "0", required = false) int pageNo,
//                                @RequestParam(value = "pageSize", defaultValue = "6",required = false) int pageSize)
//    {
//        Page<Product> pageProduct = productService.getAllProduct(pageNo,pageSize);
//        List<Setting> category = settingRepository.findAllBySettingType("category");
//        List<Setting> brand = settingRepository.findAllBySettingType("brand");
//        model.addAttribute("pageProduct", pageProduct);
//        model.addAttribute("category", category);
//        model.addAttribute("brand", brand);
//        return "shop/public_page";
//    }

    @GetMapping("/public_page")
    public String getPublicPage(Model model,
                                  @RequestParam(name = "pageNo", defaultValue = "0") int pageNo,
                                  @RequestParam(value = "pageSize", defaultValue = "6",required = false) int pageSize,
                                  @RequestParam(name = "brand" , required = false) String brand,
                                  @RequestParam(name = "category" , required = false) String category,
                                  @RequestParam(name = "search" , required = false) String search) {
        if (brand == null) {
            brand = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        if (category == null) {
            category = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        if (search == null) {
            search = ""; // Hoặc có thể gán giá trị mặc định khác tùy thuộc vào logic của bạn
        }
        List<Setting> categorys = settingRepository.findAllBySettingType("category");
        List<Setting> brands = settingRepository.findAllBySettingType("brand");
        Page<Product> productPage = productService.findProductsByType(pageNo,pageSize, brand, category, search);
        model.addAttribute("search", search);
        model.addAttribute("category", category);
        model.addAttribute("brand", brand);
        model.addAttribute("categorys", categorys);
        model.addAttribute("brands", brands);
        model.addAttribute("pageProduct", productPage);
        return "shop/public_page";
    }

}
