package com.swd.online_shop.mapper;

import com.swd.online_shop.dto.Account;
import com.swd.online_shop.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User userFromAccount(Account account) {
        if (account == null) {
            return null;
        }

        User user = new User();
        user.setUserName(account.getName());
        user.setEmail(account.getEmail());
        user.setPassword(account.getPassword());
        return user;
    }

    public Account useToAccount(User user) {
        if (user == null) {
            return null;
        }

        Account account = new Account();
        account.setEmail(user.getEmail());
        account.setPassword(user.getPassword());
        account.setName(user.getUserName());
        return account;
    }
}
