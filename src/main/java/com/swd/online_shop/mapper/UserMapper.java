package com.swd.online_shop.mapper;

import com.swd.online_shop.dto.Account;
import com.swd.online_shop.entity.User;
import org.springframework.stereotype.Component;


@Component
public interface UserMapper {
    User userFromAccount(Account account);

    Account useToAccount(User user);
}
