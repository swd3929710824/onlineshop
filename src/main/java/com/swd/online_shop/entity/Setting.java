package com.swd.online_shop.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "settings")
public class Setting {
    @Id
    @Column(name = "setting_id", nullable = false)
    private Integer id;

    @Size(max = 10)
    @NotNull
    @Column(name = "status", nullable = false, length = 10)
    private String status;

    @NotNull
    @Lob
    @Column(name = "setting_type", nullable = false)
    private String settingType;

    @Size(max = 50)
    @NotNull
    @Column(name = "setting_name", nullable = false, length = 50)
    private String settingName;

}