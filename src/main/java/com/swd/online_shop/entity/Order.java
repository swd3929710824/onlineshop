package com.swd.online_shop.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "`order`")
public class Order {
    @Id
    @Column(name = "order_id", nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Size(max = 20)
    @Column(name = "full_name", length = 20)
    private String fullName;

    @Size(max = 10)
    @Column(name = "phone", length = 10)
    private String phone;

    @Size(max = 30)
    @Column(name = "email", length = 30)
    private String email;

    @Size(max = 100)
    @Column(name = "address", length = 100)
    private String address;

    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "order_date")
    private Instant orderDate;

    @ColumnDefault("'pending'")
    @Lob
    @Column(name = "status")
    private String status;

    @Column(name = "total", precision = 10, scale = 2)
    private BigDecimal total;

    @Lob
    @Column(name = "method")
    private String method;

}