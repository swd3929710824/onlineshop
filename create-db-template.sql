CREATE DATABASE IF NOT EXISTS ONLINE_SHOPPING
    DEFAULT CHARACTER SET = 'utf8mb4';

USE ONLINE_SHOPPING;

CREATE TABLE IF NOT EXISTS `settings` (
    setting_id INT AUTO_INCREMENT PRIMARY KEY,
    status VARCHAR(10) NOT NULL,
    setting_type ENUM('role', 'category', 'brand') NOT NULL,
    setting_name VARCHAR(50) NOT NULL,
    UNIQUE (setting_type, setting_name)
);

CREATE TABLE IF NOT EXISTS `user` (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(10),
    address VARCHAR(100),
    image VARCHAR(100),
    status VARCHAR(10),
    dateofbirth DATE,
    role_id INT,
    FOREIGN KEY (role_id) REFERENCES `settings`(setting_id)
);

CREATE TABLE IF NOT EXISTS `product` (
    product_id INT AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(100) NOT NULL,
    product_image VARCHAR(100) NOT NULL,
    category_id INT,
    brand_id INT,
    description TEXT,
    price DECIMAL(10, 2) NOT NULL,
    status VARCHAR(10) NOT NULL,
    stock INT NOT NULL,
    FOREIGN KEY (category_id) REFERENCES `settings`(setting_id),
    FOREIGN KEY (brand_id) REFERENCES `settings`(setting_id)
);

CREATE TABLE IF NOT EXISTS `order` (
    order_id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    full_name VARCHAR(20),
    phone VARCHAR(10),
    email VARCHAR(30),
    address VARCHAR(100),
    order_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    status ENUM('pending', 'processing', 'completed', 'cancelled') DEFAULT 'pending',
    total DECIMAL(10, 2),
    method ENUM('Check Payment', 'COD'),
    FOREIGN KEY (user_id) REFERENCES `user`(user_id)
);

CREATE TABLE IF NOT EXISTS `orderdetail` (
    orderdetail_id INT AUTO_INCREMENT PRIMARY KEY,
    order_id INT,
    product_id INT,
    quantity INT NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    FOREIGN KEY (order_id) REFERENCES `order`(order_id),
    FOREIGN KEY (product_id) REFERENCES `product`(product_id)
);

CREATE TABLE IF NOT EXISTS `cart` (
    cart_id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES `user`(user_id)
);

CREATE TABLE IF NOT EXISTS `cartdetail` (
    cartdetail_id INT AUTO_INCREMENT PRIMARY KEY,
    cart_id INT,
    product_id INT,
    quantity INT NOT NULL,
    FOREIGN KEY (cart_id) REFERENCES `cart`(cart_id),
    FOREIGN KEY (product_id) REFERENCES `product`(product_id)
);

CREATE TABLE IF NOT EXISTS `payment` (
    payment_id INT AUTO_INCREMENT PRIMARY KEY,
    order_id INT,
    payment_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    amount DECIMAL(10, 2),
    method VARCHAR(50),
    status ENUM('pending', 'completed', 'cancelled') DEFAULT 'pending',
    FOREIGN KEY (order_id) REFERENCES `order`(order_id)
);


INSERT INTO `settings` (status, setting_type, setting_name)
VALUES
    ('active', 'role', 'admin'),
    ('active', 'role', 'customer'),
    ('active', 'role', 'seller'),
    ('active', 'category', 'MENS'),
    ('active', 'category', 'WOMENS'),
    ('active', 'category', 'KIDS'),
    ('active', 'category', 'SPORTWEAR'),
    ('active', 'category', 'BAGS'),
    ('active', 'category', 'SHOES'),      
    ('active', 'brand', 'Chanel'),
    ('active', 'brand', 'Gucci'),
    ('active', 'brand', 'Hermes'),
    ('active', 'brand', 'Prada'),
    ('active', 'brand', 'LouisVuitton'),
    ('active', 'brand', 'Burberry'),
    ('active', 'brand', 'Dior'),
    ('active', 'brand', 'Ralph Lauren'),
    ('active', 'brand', 'Dolce & Gabbana'),
    ('active', 'brand', 'Armani'),
    ('active', 'brand', 'Versace'),
    ('active', 'brand', 'FENDI'),
    ('active', 'brand', 'Givenchy'),
    ('active', 'brand', 'Yves Saint Laurent'),
    ('active', 'brand', 'Bottega Veneta');

INSERT INTO `user` (user_name, password, email, phone, address, image, status, dateofbirth, role_id)
VALUES
    ('admin_user', 'admin123', 'admin@example.com', '1234567890', '123 Admin St, Admin City', 'https://iili.io/dC9sYBf.png', 'active', '1990-01-01', 1),
    ('john_doe', 'john123', 'john.doe@example.com', '9876543210', '456 Main St, Some City', 'https://iili.io/dC9sYBf.png', 'active', '1995-05-15', 2),
    ('ryan_le', 'ryaan123', 'ryan_le@example.com', '0326617933', 'Ha Hoi, Vietnam', 'https://iili.io/dC9sYBf.png', 'active', '2003-03-14', 3),
    ('jane_smith', 'jane123', 'jane.smith@example.com', '8765432109', '789 Elm St, Another City', 'https://iili.io/dC9sYBf.png', 'active', '1988-11-20', 2);

    
INSERT INTO `product` (product_name, product_image, category_id, brand_id, description, price, status, stock)
VALUES
    ('Chanel 01', 'https://iili.io/dCH3bus.jpg', 4, 10, 'Latest Chanel Men model', 1099.99, 'active', 50),
    ('Chanel 02', 'https://iili.io/dCHfYCv.jpg', 5, 10, 'Latest Chanel Women model', 1999.99, 'active', 40),
    ('Gucci Bag 01', 'https://iili.io/dCHnhRS.jpg', 8, 11, 'Latest Gucci Gag', 249.99, 'active', 100),
    ('Gucci Shoes Max', 'https://iili.io/dCHxfZF.jpg', 9, 11, 'Running shoes for comfort and style', 129.99, 'active', 35),
    ('Gucci 04 Max', 'https://iili.io/dCHIGh7.jpg', 4, 11, 'Gucci Max Men model', 529.99, 'active', 35),
    ('Hermes Bag 01', 'https://iili.io/dCHAcYJ.jpg', 8, 12, 'Latest Hermes Gag', 329.99, 'active', 50),
    ('Hermes 06', 'https://iili.io/dCHReKF.jpg', 5, 12, 'Latest Hermes Women model', 729.99, 'active', 65),
    ('Hermes 07', 'https://iili.io/dCH70pj.jpg', 4, 12, 'Latest Hermes Men model', 829.99, 'active', 35),
    ('LouisVuitton Bag Max', 'https://iili.io/dCHYJae.jpg', 8, 14, 'Latest LouisVuitton Gag', 429.99, 'active', 35),
    ('LouisVuitton Shoes 02', 'https://iili.io/dCHaox4.jpg', 9, 14, 'Running shoes for comfort and style', 229.99, 'active', 35),
    ('Dior Bag 11', 'https://iili.io/dCHcxpa.jpg', 9, 16, 'Running shoes for comfort and style', 535.99, 'active', 35),
    ('Dior Shoes Max', 'https://iili.io/dCHl5tj.jpg', 9, 11, 'Running shoes for comfort and style', 129.99, 'active', 35),
    ('Dior 12', 'https://iili.io/dCHxfZF.jpg', 5, 16, 'Latest Dior Women model', 729.99, 'active', 35),
    ('Dior Man Max', 'https://iili.io/dCHVgO7.jpg', 4, 16, 'Latest Chanel Men model', 419.99, 'active', 75),
    ('Dolce & Gabbana Shoes Max', 'https://iili.io/dCHWr2R.jpg', 9, 18, 'Running shoes for comfort and style', 629.99, 'active', 35),
    ('Versace 16', 'https://iili.io/dCHhJcv.jpg', 4, 20, 'Latest Chanel Men model', 629.99, 'active', 47),
    ('Yves Saint Laurent 14', 'https://iili.io/dCHjq5Q.jpg', 4, 23, 'Yves Saint Laurent 14 Men model', 829.99, 'active', 35),                                                                                        
    ('Bottega Veneta Bag max', 'https://iili.io/dCHNCV1.jpg', 8, 24, 'Bottega Veneta Bag model', 1049.99, 'active', 80);
INSERT INTO `order` (user_id, full_name, phone, email, address, order_date, status, total, method)
VALUES
    (3, 'John Doe', '9876543210', 'john.doe@example.com', '456 Main St, Some City', '2023-06-15 10:30:00', 'completed', 3099.98, 'Check Payment'),
    (4, 'Ryan Le', '0326617933', 'ryan_le@example.com', 'Ha Hoi, Vietnam', '2023-06-16 12:00:00', 'completed', 249.99, 'COD');

INSERT INTO `orderdetail` (order_id, product_id, quantity, price)
VALUES
    (1, 1, 1, 1099.99),
    (1, 2, 1, 1999.99),
    (2, 3, 1, 249.99);

-- Assuming cart_id 1 belongs to user_id 2 (John Doe)
INSERT INTO `cart` (cart_id, user_id, created_at)
VALUES
    (1, 2, '2023-06-15 10:00:00');

-- Items in John Doe's cart
INSERT INTO `cartdetail` (cart_id, product_id, quantity)
VALUES
    (1, 1, 1),
    (1, 5, 2);
INSERT INTO `payment` (order_id, payment_date, amount, method, status)
VALUES
    (1, '2023-06-15 10:45:00', 1349.98, 'Payment', 'completed'),
    (2, '2023-06-16 12:30:00', 299.98, 'COD', 'completed');
